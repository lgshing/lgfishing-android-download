### GFishing
老G钓鱼-Android APP



### App下载 <br/>
[lgfishing_v1.2.1.apk](http://47.100.13.178:8881/file/downloadAndroid)<br/>
<br/>
二维码下载<br/>
<br/>
![app下载 v1.1.7](1.1.7.png)


### 更新日志
-------------------

>**版本** : *1.2.1* &nbsp;&nbsp;&nbsp;&nbsp;
>**Android code** : *10* &nbsp;&nbsp;&nbsp;&nbsp;
**分支** : *master* &nbsp;&nbsp;&nbsp;&nbsp;
**环境** : *prod*   &nbsp;&nbsp;&nbsp;&nbsp;
**时间** : *2018-7-12 00:25:10*
>
>**更新内容：**
```
•优化分享-分享加积分
•服务器优化
•发帖优化-发帖加积分
```

-------------------


>**版本** : *1.2.0* &nbsp;&nbsp;&nbsp;&nbsp;
>**Android code** : *9* &nbsp;&nbsp;&nbsp;&nbsp;
**分支** : *master* &nbsp;&nbsp;&nbsp;&nbsp;
**环境** : *prod*   &nbsp;&nbsp;&nbsp;&nbsp;
**时间** : *2018-6-15 23:17:45*
>
>**更新内容：**
```
•切换到新服务器，参数加密请求，请求协议升级到https
•修复部分机型无法更新
•修复未登录崩溃
•优化用户体验
```
-------------------
>**版本** : *1.1.7* &nbsp;&nbsp;&nbsp;&nbsp;
**分支** : *master* &nbsp;&nbsp;&nbsp;&nbsp;
**环境** : *prod*   &nbsp;&nbsp;&nbsp;&nbsp;
**时间** : *2018-5-8 23:38:47*
>
>**更新内容：**
```
•为了更好的用户体验，我们将登录变更为原来的手机号码登录+第三方登录
•新增积分排行榜，精美奖品等着你
•新增消息推送，热门资讯即刻到达
•如果更新失败，请卸载重新安装
下载链接：http://47.100.13.178:8881/file/downloadAndroid
```
-----------------------------
>**版本** : *1.1.6* &nbsp;&nbsp;&nbsp;&nbsp;
**分支** : *master* &nbsp;&nbsp;&nbsp;&nbsp;
**环境** : *prod*   &nbsp;&nbsp;&nbsp;&nbsp;
**时间** : *2018-3-22 15:17:03*
>
>**更新内容：**
```
•修复打开帖子详情崩溃
•优化分享图标
•优化发布帖子
```
-----------------------------

>**版本** : *1.1.5* &nbsp;&nbsp;&nbsp;&nbsp;
**分支** : *master* &nbsp;&nbsp;&nbsp;&nbsp;
**环境** : *prod*
>
>**更新内容：**
```
•直接使用微信进行登录
•优化商品店铺帖子详情
•优化分享帖子
•修复帖子评论
•优化用户体验
```
-----------------------------



